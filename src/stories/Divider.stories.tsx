import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Divider } from "../components";

export default {
  title: "Components/Divider",
  component: Divider,
  parameters: {
    layout: "fullscreen",
  },
} as ComponentMeta<typeof Divider>;

const Template: ComponentStory<typeof Divider> = (args) => (
  <Divider {...args}>tes </Divider>
);

export const Main = Template.bind({});

Main.args = {};
