import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Datepicker } from "../components";

export default {
  title: "Components/Datepicker",
  component: Datepicker,
  parameters: {
    layout: "fullscreen",
  },
} as ComponentMeta<typeof Datepicker>;

const Template: ComponentStory<typeof Datepicker> = (args) => (
  <Datepicker {...args} />
);

export const Main = Template.bind({});

Main.args = {};
