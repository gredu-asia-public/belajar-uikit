import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Button, Modal } from "../components";

export default {
  title: "Components/Modal",
  component: Modal,
  parameters: {
    layout: "fullscreen",
  },
} as ComponentMeta<typeof Modal>;

const Template: ComponentStory<typeof Modal> = (args) => <Modal {...args} />;

export const Main = Template.bind({});

Main.args = {
  visible: true,
  footer: <Button>test</Button>,
};
