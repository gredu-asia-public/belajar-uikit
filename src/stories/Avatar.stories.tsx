import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Avatar } from "../components";

export default {
  title: "Components/Avatar",
  component: Avatar,
  parameters: {
    layout: "fullscreen",
  },
} as ComponentMeta<typeof Avatar>;

const Template: ComponentStory<typeof Avatar> = (args) => <Avatar {...args} />;

export const Main = Template.bind({});

Main.args = {};
