import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { EmptyState } from "../components";

export default {
  title: "Components/EmptyState",
  component: EmptyState,
  parameters: {
    layout: "fullscreen",
  },
} as ComponentMeta<typeof EmptyState>;

const Template: ComponentStory<typeof EmptyState> = (args) => (
  <EmptyState {...args} />
);

export const Main = Template.bind({});

Main.args = {};
