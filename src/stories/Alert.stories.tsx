import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Alert } from "../components";

export default {
  title: "Components/Alert",
  component: Alert,
  parameters: {
    layout: "fullscreen",
  },
} as ComponentMeta<typeof Alert>;

const Template: ComponentStory<typeof Alert> = (args) => <Alert {...args} />;

export const Main = Template.bind({});

Main.args = {};
