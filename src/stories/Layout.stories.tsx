import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { MainLayout } from "../components/";

export default {
  title: "Components/Layout",
  component: MainLayout,
  parameters: {
    layout: "fullscreen",
  },
} as ComponentMeta<typeof MainLayout>;

const Template: ComponentStory<typeof MainLayout> = (args) => (
  <MainLayout {...args} />
);

export const Main = Template.bind({});

Main.args = {
  children: <div>123</div>,
  sideBar: <div>123</div>,
  withFooter: true,
};
