import type { TypoIndex } from './typo';

export type ButtonSizeIndex = 'xs' | 'sm' | 'md' | 'lg' | 'xl';

type ButtonSizeCollection = {
  fontStyle: TypoIndex;
  padding: string;
};

export type ButtonSize = {
  xs: ButtonSizeCollection;
  sm: ButtonSizeCollection;
  md: ButtonSizeCollection;
  lg: ButtonSizeCollection;
  xl: ButtonSizeCollection;
};
