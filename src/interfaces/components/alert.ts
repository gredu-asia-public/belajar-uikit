import type { AlertProps as AntdAlertProps } from "antd";
import type { Color, ColorIndex } from "../colors";

export type AlertType = "success" | "info" | "warning" | "danger";

export type AlertVariantCollection = {
  backgroundColor: Color;
  iconColor: Color;
  borderColor: Color;
  textColor: ColorIndex;
  closeIconColor: Color;
};

export interface AlertProps extends Omit<AntdAlertProps, "type"> {
  title: string;
  showIcon?: boolean;
  onClose?: () => void;
  variant: AlertType;
  description?: string;
  banner?: boolean;
  closable?: boolean;
}

export interface StyledAlertProps extends Omit<AntdAlertProps, "type"> {
  variant: AlertType;
}

export type AlertVariant = {
  warning: AlertVariantCollection;
  success: AlertVariantCollection;
  danger: AlertVariantCollection;
  info: AlertVariantCollection;
};
