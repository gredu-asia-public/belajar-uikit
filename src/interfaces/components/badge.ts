import { BadgeProps as AntdBadgeProps } from 'antd';
import { BadgeStateIndex } from 'interfaces/constants/badgeState';

export interface BadgeProps extends AntdBadgeProps {
  state?: BadgeStateIndex;
}
