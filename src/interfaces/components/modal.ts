import React from "react";
import { ModalFuncProps } from "antd";

import type { ModalProps as AntdModalProps } from "antd";
import type { ModalSizeIndex } from "../../interfaces/constants/modalSize";
import type { LabelVariantIndex } from "../../interfaces/constants/labelVariant";
import type { LabelSizeIndex } from "../../interfaces/constants/labelSize";

export interface ModalProps
  extends Omit<
    AntdModalProps,
    "title" | "closable" | "closeItem" | "okType" | "maskStyle"
  > {
  subTitle?: string;
  title?: string | React.ReactNode;
  size?: ModalSizeIndex;
  withHeader?: boolean;
  withClose?: boolean;
  icon?: React.ReactNode;
  label?: {
    type: LabelVariantIndex;
    text: string;
    size: LabelSizeIndex;
  };
}

export interface StyledModalProps extends AntdModalProps {
  subTitle?: string;
}

export interface ModalHeader {
  title: React.ReactNode;
  subTitle?: string;
  size?: ModalSizeIndex;
  icon?: React.ReactNode;
  label?: {
    type: LabelVariantIndex;
    text: string;
    size: LabelSizeIndex;
  };
}

export interface ModalConfirmProps extends ModalFuncProps {
  height?: number | string;
}
