import type { ColProps } from 'antd';

export interface ColumnProps extends ColProps {
  children?: React.ReactNode;
}
