import { TooltipPropsWithTitle as AntdTooltipProps } from 'antd/lib/tooltip';

export type TooltipType = 'light' | 'dark';
export interface TooltipProps extends AntdTooltipProps {
  type?: TooltipType;
  headerTitle?: string;
  width?: number | string;
  maxWidth?: number;
}
