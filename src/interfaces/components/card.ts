export type CardProps = {
  children: React.ReactNode;
  withRadius?: boolean;
  bordered?: boolean;
  withBoxShadow?: boolean;
  className?: string;
  id?: string;
  width?: string;
  padding?: string;
  cursor?: string;
};
