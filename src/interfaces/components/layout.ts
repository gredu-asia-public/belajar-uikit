export interface MainLayoutProps {
  header?: React.ReactNode;
  sideBar?: React.ReactNode;
  withFooter?: boolean;
  sideBarWidth?: string;
  children: React.ReactNode;
}
