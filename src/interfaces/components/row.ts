import type { RowProps as RowPropsAntd } from 'antd';

export interface RowProps extends RowPropsAntd {
  children: React.ReactNode;
}
