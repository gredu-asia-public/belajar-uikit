import type {
  NotificationApi,
  NotificationPlacement,
} from 'antd/lib/notification';
import type { Color, ColorIndex } from 'interfaces/colors';

type SnackBarIndex = 'info' | 'warning' | 'danger' | 'success' | 'neutral';

type SnackBarVariantCollection = {
  backgroundColor: Color;
  iconColor: Color;
  borderColor: Color;
  textColor: ColorIndex;
  closeIconColor: Color;
};
export interface SnackBarProps {
  title: string;
  showIcon?: boolean;
  placement?: NotificationPlacement;
  onClickButton?: () => void;
  onClose?: () => void;
  type: SnackBarIndex;
  buttonText?: string;
  description?: string;
}
export interface AlertProps extends SnackBarProps {
  iconColor: Color;
  textColor: ColorIndex;
  closeIconColor: Color;
  notification: NotificationApi;
  key: string;
  buttonText?: string;
}

export interface WrappedProps {
  showIcon: boolean;
}

export interface StyledSnackBarProps {
  hasButton?: boolean;
}

export type snackBarVariant = {
  warning: SnackBarVariantCollection;
  success: SnackBarVariantCollection;
  danger: SnackBarVariantCollection;
  info: SnackBarVariantCollection;
  neutral: SnackBarVariantCollection;
};
