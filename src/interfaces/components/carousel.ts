import type { CarouselProps as CarouselPropsAntd } from 'antd';

export interface CarouselProps extends CarouselPropsAntd {
  children: React.ReactNode;
}
