import React from "react";

import type { ButtonProps as AntdButtonProps } from "antd";

import type { Variant as ButtonVariant } from "../buttonVariant";
import type { ButtonSizeIndex } from "../buttonSize";

export interface BaseButtonProps
  extends Omit<
    AntdButtonProps,
    "size" | "type" | "loading" | "prefix" | "suffix" | "icon"
  > {
  size?: "xs" | "sm" | "md" | "lg" | "xl";
  type?: ButtonVariant;
  loading?: boolean;
  isDisabled?: boolean;
  prefix?: React.ReactNode;
  suffix?: React.ReactNode;
}

export interface ButtonProps extends BaseButtonProps {
  onClick?: () => void;
  children: React.ReactNode;
}

export interface StyledButtonProps
  extends Omit<AntdButtonProps, "type" | "size" | "loading" | "icon"> {
  loading?: boolean;
  disabled: boolean;
  size: "xs" | "sm" | "md" | "lg" | "xl";
  type: ButtonVariant;
}

export interface StyledButtonIconProps
  extends React.ButtonHTMLAttributes<HTMLElement> {
  size?: ButtonSizeIndex;
  variant?: ButtonVariant;
}
