import { DividerProps as AntdDividerProps } from 'antd/lib/divider';

export type BorderSize = '1px' | '8px';

export interface DividerProps extends AntdDividerProps {
  children?: React.ReactNode;
  size?: BorderSize;
  margin?: string;
}
