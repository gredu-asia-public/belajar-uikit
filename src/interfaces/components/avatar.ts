import type { AvatarProps as AntdAvatarProps } from "antd";
import type { AvatarIndex } from "../constants/avatarSize";

export interface AvatarProps extends Omit<AntdAvatarProps, "size"> {
  size: AvatarIndex;
}
