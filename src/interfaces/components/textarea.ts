import { TextAreaProps as AntdTextAreaProps } from 'antd/lib/input/TextArea';

export interface TextAreaProps extends Omit<AntdTextAreaProps, 'onChange'> {
  value?: string;
  onChange?: (value: string) => void;
  name?: string;
  isError?: boolean;
}
