import type { Color } from "./colors";

export type Variant =
  | "primary"
  | "secondary"
  | "tertiary"
  | "link"
  | "danger"
  | "naked";

type VariantCollection = {
  color: Color;
  boxShadow?: string;
  fontColor?: Color;
  border?: string;
};

export interface ButtonCollection {
  state: {
    loading: VariantCollection;
    disabled: VariantCollection;
  };
  default: VariantCollection;
  hover: VariantCollection;
  pressed: VariantCollection;
}

export interface ButtonVariant {
  primary: ButtonCollection;
  secondary: ButtonCollection;
  tertiary: ButtonCollection;
  link: ButtonCollection;
  danger: ButtonCollection;
  naked: ButtonCollection;
}
export default ButtonVariant;
