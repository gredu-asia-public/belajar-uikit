export type AvatarIndex =
  | 'xs'
  | 'sm'
  | 'md'
  | 'lg'
  | 'xl'
  | 'xxl'
  | 'profileMd'
  | 'profileLg';

export type AvaSize =
  | '24px'
  | '32px'
  | '40px'
  | '48px'
  | '56px'
  | '64px'
  | '96px'
  | '160px';

export type AvaIconSize = 16 | 20 | 24 | 28 | 32 | 36 | 48 | 60;

export type AvatarCollection = {
  size: AvaSize;
  iconSize: AvaIconSize;
  border?: string;
  withShadow?: boolean;
};

export type AvatarSize = {
  xs: AvatarCollection;
  sm: AvatarCollection;
  md: AvatarCollection;
  lg: AvatarCollection;
  xl: AvatarCollection;
  xxl: AvatarCollection;
  profileMd: AvatarCollection;
  profileLg: AvatarCollection;
};
