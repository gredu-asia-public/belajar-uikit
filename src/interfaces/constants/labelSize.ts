import type { TypoIndex } from "../typo";

export type LabelSizePadding = "2px 8px" | "2px 10px" | "4px 12px";

export type LabelSizeIndex = "sm" | "md" | "lg";

export type LabelSizeCollection = {
  padding: LabelSizePadding;
  fontStyle: TypoIndex;
};

export type LabelSize = {
  sm: LabelSizeCollection;
  md: LabelSizeCollection;
  lg: LabelSizeCollection;
};
