import type { Color } from "../colors";

export type BadgeState = {
  default: Color;
  warning: Color;
  disclaimer: Color;
  info: Color;
  error: Color;
  primaryStudent: Color;
};

export type BadgeStateIndex =
  | "default"
  | "warning"
  | "disclaimer"
  | "info"
  | "error"
  | "primaryStudent";
