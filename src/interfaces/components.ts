export interface EmptyStateProps {
  title?: string;
  description?: string;
  buttonMessage?: string;
  buttonType?: 'primary' | 'tertiary' | 'secondary';
  type?:
    | 'emptyState1'
    | 'emptyState2'
    | 'errorState1'
    | 'errorState2'
    | 'success1'
    | 'success2'
    | 'welcome'
    | 'loading';
  onButtonClick?: () => void;
}
