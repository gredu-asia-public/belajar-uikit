import moment from "moment";
require("moment/locale/id");

const DEFAULT_FORMAT = "YYYY-MM-DD";
const DATE_LABEL_FORMAT = "dddd, DD MMM YYYY HH:mm";

moment.locale("id");

const date = (
  date: Date | string | number | undefined,
  format: string = DEFAULT_FORMAT
): string => {
  if (!date || !moment(date).isValid()) return "";
  return moment(date).format(format);
};

const getYear = () => {
  return moment().year();
};

export default { date, getYear };
