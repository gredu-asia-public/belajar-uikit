import type { Locale } from 'rc-picker/lib/interface';
declare const locale: Locale;
export default locale;
