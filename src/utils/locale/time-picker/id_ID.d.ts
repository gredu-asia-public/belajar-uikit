import type { TimePickerLocale } from 'antd/lib/time-picker/index';
declare const locale: TimePickerLocale;
export default locale;
