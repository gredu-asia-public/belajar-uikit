import type { PickerLocale } from 'antd/lib/date-picker/generatePicker';
declare const locale: PickerLocale;
export default locale;
