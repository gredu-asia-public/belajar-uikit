import { css } from 'styled-components';

import { InputSizeType } from '../../interfaces/inputSize';
import { inputSize } from '../../constants/index';

export const getInputSize = (size: InputSizeType) => {
  return css`
    ${inputSize[size]}
  `;
};
