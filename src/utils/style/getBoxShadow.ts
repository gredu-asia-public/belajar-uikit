import boxShadow from "../../constants/boxShadow";

import type { boxShadowType } from "../../interfaces/getBoxShadow";

const getBoxShadow = (type: boxShadowType) => {
  switch (type) {
    case boxShadow?.[type]?.key:
      return boxShadow?.[type]?.value || "unset";
    default:
      return boxShadow?.default?.value;
  }
};

export default getBoxShadow;
