import { css } from "styled-components";

import colors from "../../constants/colors";
import modalSize from "../../constants/modalSize";

import type { ModalSizeIndex } from "interfaces/constants/modalSize";

const getModalSize = (size: ModalSizeIndex = "md") => {
  return css`
    &.ant-modal {
      width: ${modalSize[size].bodyWidth} !important;
    }
    .ant-modal-header {
      padding: ${modalSize[size].headerPadding};
      background-color: ${colors.n50};
      border-bottom: none;
    }
    .ant-modal-body {
      padding: 16px 24px;
    }
    .ant-modal-footer {
      display: flex;
      justify-content: flex-end;
      column-gap: 8px;
      border-top: 2px solid ${colors.n200};
      padding: ${modalSize[size].footerPadding};
    }
  `;
};

export default getModalSize;
