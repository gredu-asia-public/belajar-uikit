import React from "react";
import { Button, Icon, Text } from "../index";

import { ButtonWrapper, EmptyStateContainer, Wrapper } from "./style";

import type { EmptyStateProps } from "../../interfaces/components/emptystate";
import type { IcomoonType } from "../../interfaces/icomoonType";

const EmptyState: React.FC<EmptyStateProps> = ({
  title,
  description,
  buttonMessage,
  type = "emptyState1",
  buttonType = "primary",
  onButtonClick,
}) => {
  const getEmptyStateImage = (type: string) => {
    let iconName: IcomoonType = "no_file";
    switch (type) {
      case "emptyState1":
        iconName = "no_file";
        break;
      case "emptyState2":
        iconName = "no_search";
        break;
      case "error1":
        iconName = "page_notfound";
        break;
      case "error2":
        iconName = "server_error";
        break;
      case "welcome":
        iconName = "paper_plane";
        break;
      case "success1":
        iconName = "success_hand";
        break;
      case "success2":
        iconName = "success_check";
        break;
      default:
        iconName = "no_file";
        break;
    }
    return <Icon name={iconName} size="100%" />;
  };

  return (
    <EmptyStateContainer>
      <Wrapper>{getEmptyStateImage(type)}</Wrapper>
      <Text type="bodyDefault16Bold" className="mt-5 pt-5">
        {title}
      </Text>
      {description && (
        <Wrapper>
          <Text
            className="mt-2"
            type="bodyDefault14"
            color="n500"
            align="center"
          >
            {description}
          </Text>
        </Wrapper>
      )}
      {buttonMessage && (
        <ButtonWrapper className="mt-5">
          <Button onClick={onButtonClick} type={buttonType}>
            {buttonMessage}
          </Button>
        </ButtonWrapper>
      )}
    </EmptyStateContainer>
  );
};

export default EmptyState;
