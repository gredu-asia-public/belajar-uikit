import styled, { css } from "styled-components";
import { Tag } from "antd";

import type { LabelProps } from "../../interfaces/components/label";
import type { LabelSizeIndex } from "../../interfaces/constants/labelSize";
import type { LabelVariantIndex } from "../../interfaces/constants/labelVariant";

import { typo } from "../../constants/index";
import labelSize from "../../constants/labelSize";
import { labelVariant } from "../../constants/labelVariant";
import getBorderRadius from "../../utils/style/getBorderRadius";

const getLabelSize = ({ size = "md" }: { size?: LabelSizeIndex }) => css`
  padding: ${labelSize[size].padding};
  font-size: ${typo[labelSize[size].fontStyle].size} !important;
  font-weight: ${typo[labelSize[size].fontStyle].weight} !important;
  line-height: ${typo[labelSize[size].fontStyle].lineHeight} !important;
  letter-spacing: ${typo[labelSize[size].fontStyle].letterSpacing} !important;
`;

const getLabelVariant = ({
  type = "default",
}: {
  type?: LabelVariantIndex;
}) => css`
  background: ${labelVariant[type].color} !important;
  color: ${labelVariant[type].fontColor};
  border: ${labelVariant[type].border} !important;
`;

export const StyledLabel = styled(Tag)<LabelProps>`
  ${getLabelSize}
  ${getLabelVariant}
  ${getBorderRadius("label")}
  width: fit-content;
`;
