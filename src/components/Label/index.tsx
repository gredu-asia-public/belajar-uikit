import React from "react";

import { StyledLabel } from "./styled";

import type { LabelProps } from "../../interfaces/components/label";

const Label: React.FC<LabelProps> = ({ size, children, type, ...restProp }) => {
  return (
    <StyledLabel size={size} type={type} {...restProp}>
      {children}
    </StyledLabel>
  );
};

export default Label;
