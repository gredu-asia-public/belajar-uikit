import * as React from "react";

import { Text, FlexBox } from "../index";
import dateFormat from "../../utils/dateFormat";
import GreduLogo from "../../public/assets/greduLogo.svg";

import {
  MainWrapper,
  HeaderWrapper,
  ContentWrapper,
  FooterWrapper,
  SidebarWrapper,
  InnerContentWrapper,
} from "./styled";

import type { MainLayoutProps } from "../../interfaces/components/layout";

const defaultHeader = (
  <FlexBox alignItems="center">
    <img src={GreduLogo} alt="" />
  </FlexBox>
);

const MainLayout: React.FC<MainLayoutProps> = ({
  header = defaultHeader,
  children,
  sideBar,
  sideBarWidth,
  withFooter = false,
}) => {
  return (
    <MainWrapper>
      {header && <HeaderWrapper>{header}</HeaderWrapper>}
      {sideBar && (
        <SidebarWrapper sideBarWidth={sideBarWidth}>{sideBar}</SidebarWrapper>
      )}
      <ContentWrapper sideBarWidth={sideBarWidth} sideBar={sideBar}>
        <InnerContentWrapper sideBarWidth={sideBarWidth}>
          {children}
        </InnerContentWrapper>
        {withFooter && (
          <FooterWrapper>
            <div>
              <Text type="caption" color="n500">
                {`Powered by `}
              </Text>
              <Text type="captionBold" color="n400">
                GREDU
              </Text>
            </div>
            <Text type="caption" color="n400">
              {`Copyright © (${dateFormat.getYear()}) Gredu Asia. All rights reserved. - GREDU PT. Sumber Kreatif Indonesia.`}
            </Text>
          </FooterWrapper>
        )}
      </ContentWrapper>
    </MainWrapper>
  );
};

export default MainLayout;
