import styled, { StyledComponent } from "styled-components";
import { Layout } from "antd";

import colors from "../../constants/colors";
import getBoxShadow from "../../utils/style/getBoxShadow";

type sidebarWidthProps = {
  sideBarWidth: string;
  sideBar?: React.ReactNode;
};

export const MainWrapper = styled.div`
  display: flex;
  width: 100%;
  padding-top: 56px;
`;

export const SidebarWrapper: StyledComponent<
  any,
  any,
  sidebarWidthProps
> = styled.div<sidebarWidthProps>`
  width: ${(props) => props.sideBarWidth || "210px"};
  border-right: 1px solid ${colors.n150};
`;

export const HeaderWrapper = styled(Layout.Header)`
  background-color: ${colors.n50};
  box-shadow: ${() => getBoxShadow("default")};
  border: ${() => `1px solid ${colors.n150}`};
  z-index: 1000;
  position: fixed;
  width: 100%;
  padding: 0 3.2rem;
  height: 56px;
  top: 0;
  left: 0;
`;

export const ContentWrapper: StyledComponent<
  any,
  any,
  sidebarWidthProps
> = styled.div<sidebarWidthProps>`
  background-color: ${colors.n0};
  width: ${(props) =>
    props.sideBar ? `calc(100% - ${props.sideBarWidth || "210px"})` : "100%"};
`;

export const InnerContentWrapper: StyledComponent<
  any,
  any,
  sidebarWidthProps
> = styled(Layout.Content) <sidebarWidthProps>`
  background-color: ${colors.n0};
  min-height: calc(100vh - (56px * 2));
  display: flex;
  flex-direction: column;
  padding: 3.2rem 3.2rem 0;
`;

export const FooterWrapper = styled(Layout.Footer)`
  background-color: ${colors.n0};
  text-align: center;
  padding: 1.2rem 2.4rem 2.4rem;
`;
