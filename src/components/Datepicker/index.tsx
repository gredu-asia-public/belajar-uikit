import React from "react";
import { DatePicker, DatePickerProps } from "antd";
import "moment/locale/id";
import moment from "moment";

import localeID from "../../utils/locale/id_ID";

const FORMAT_DATE = "DD MMM YYYY";
moment.locale("id", {
  week: {
    dow: 1,
  },
  weekdaysMin: ["Ming", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
});

const Datepicker: React.FC<DatePickerProps> = ({ format, locale, ...rest }) => {
  return (
    <DatePicker
      locale={locale || localeID.DatePicker}
      format={format || FORMAT_DATE}
      {...rest}
    />
  );
};

export default Datepicker;
