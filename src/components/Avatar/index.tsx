import React from "react";
import { isEmpty } from "lodash";

import Icon from "../Icon";
import { avatarSize, colors } from "../../constants/index";

import { AvatarContainer, StyledAvatar } from "./styled";

import type { AvatarProps } from "../../interfaces/components/avatar";

const Avatar: React.FC<AvatarProps> = ({ size = "sm", src }) => {
  const srcUrl = !isEmpty(src) ? src : undefined;
  return (
    <AvatarContainer>
      <StyledAvatar
        size={size}
        src={srcUrl}
        icon={
          <Icon
            name="person"
            color={colors.p500}
            size={avatarSize[size].iconSize}
            type="external"
          />
        }
      />
    </AvatarContainer>
  );
};

export default Avatar;
