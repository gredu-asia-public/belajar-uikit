import React from "react";
import { Tooltip } from "antd";

import getBoxShadow from "../../utils/style/getBoxShadow";
import colors from "../../constants/colors";
import borderRadius from "../../constants/borderRadius";

import type { TooltipProps } from "../../interfaces/components/tooltip";

const TooltipComponent: React.FC<TooltipProps> = ({
  title,
  placement,
  children,
  type = "light",
  width,
  maxWidth,
  ...restProps
}) => {
  return (
    <Tooltip
      title={title}
      placement={placement}
      overlayInnerStyle={{
        color: type === "light" ? colors.n900 : colors.n0,
        boxShadow: getBoxShadow("default"),
        borderRadius: borderRadius.default,
        ...(width && { width }),
        ...(maxWidth && { maxWidth }),
      }}
      color={type === "light" ? colors.n0 : colors.n900}
      {...restProps}
    >
      {children}
    </Tooltip>
  );
};

export default TooltipComponent;
