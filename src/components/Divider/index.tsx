import React from "react";

import StyledDivider from "./styled";

import type {
  BorderSize,
  DividerProps,
} from "../../interfaces/components/divider";

const Divider: React.FC<DividerProps> = ({
  children,
  size = "1px",
  margin,
  ...restProps
}) => {
  return (
    <StyledDivider size={size as BorderSize} margin={margin} {...restProps}>
      {children}
    </StyledDivider>
  );
};

export default Divider;
