import styled from "styled-components";
import { Divider } from "antd";

import { colors } from "../../constants/index";

import type { DividerProps } from "../../interfaces/components/divider";

const StyledDivider = styled(Divider)<DividerProps>`
  border-top-width: ${(props) => props.size};
  border-color: ${colors.n200};
  margin: ${(props) => props.margin};
`;

export default StyledDivider;
