import React from "react";
import { Popover as AntdPopover } from "antd";
import { Text } from "../index";

import type { PopoverProps as AntdPopoverProps } from "antd";

interface PopoverProps extends Omit<AntdPopoverProps, "title"> {
  title?: string;
  className?: string;
}

const Popover: React.FC<PopoverProps> = ({
  children,
  title,
  className = "custom-popover-components",
  ...restProps
}) => {
  return (
    <AntdPopover
      title={title && <Text type="bodyDefault14SemiBold">{title}</Text>}
      overlayClassName={className}
      {...restProps}
    >
      {children}
    </AntdPopover>
  );
};

export default Popover;
