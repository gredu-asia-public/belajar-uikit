import { Badge } from "antd";

import { badgeState } from "../../constants/badgeState";

import styled, { css, StyledComponent } from "styled-components";

import type { BadgeStateIndex } from "../../interfaces/constants/badgeState";
import type { BadgeProps } from "../../interfaces/components/badge";

const getBadgeState = ({ state }: { state?: BadgeStateIndex }) => css`
  background-color: ${badgeState[state as BadgeStateIndex]};
`;

export const StyledBadge: StyledComponent<any, any, BadgeProps> = styled(
  Badge
)<BadgeProps>`
  sup {
    ${getBadgeState}
    top: -3px;
    right: -3px;
  }
`;
