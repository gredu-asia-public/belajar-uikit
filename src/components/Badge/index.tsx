import React from "react";

import { StyledBadge } from "./styled";

import { BadgeProps } from "../../interfaces/components/badge";

const Badge: React.FC<BadgeProps> = ({
  children,
  state = "primaryStudent",
  count,
  dot,
}) => {
  return (
    <StyledBadge state={state} dot={dot} count={count}>
      {children}
    </StyledBadge>
  );
};

export default Badge;
