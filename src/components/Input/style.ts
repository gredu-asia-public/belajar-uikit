import React from 'react';
import styled from 'styled-components';
import { Input, InputProps } from 'antd';

import colors from '../../constants/colors';
import getBoxShadow from '../../utils/style/getBoxShadow';
import { getBorderRadius, getInputSize, getFontStyle } from '../../utils';

import { SizeType } from './type';

interface InputStyleProps extends Omit<InputProps, 'size'> {
  width?: string;
  isError?: boolean;
  background?: string;
  sizeType: SizeType;
}

export const StyledInput: React.FC = styled(Input) <InputStyleProps>`
  ${getFontStyle('bodyDefault16')}
  ${getBorderRadius()}
  border-color: ${colors.n200};
  ${(props) => getInputSize(props.sizeType)};
  &.ant-input-affix-wrapper {
    background-color: ${(props) => props.background || colors.n0};
  }
  &.ant-input-affix-wrapper > input.ant-input {
    background-color: ${(props) => props.background || colors.n0};
  }
  &.ant-input-affix-wrappexr:not(.ant-input-affix-wrapper-disabled):hover,
  &.ant-input-affix-wrapper-focused,
  .ant-input-affix-wrapper:focus {
    border: ${(props) => (props.isError ? `1px solid ${colors.s500}` : '')};
    box-shadow: ${(props) => (props.isError ? getBoxShadow('primary') : '')};
  }

  &.ant-input[disabled] {
    background-color: ${colors.n50} !important;
  }

  .ant-form-item-feedback-icon {
    margin-left: 13.5px;
  }

  width: ${(props) => props.width || '100%'};
`;
export const StyledInputPassword = styled(Input.Password) <InputStyleProps>`
  border-radius: 4px;
  height: 44px;
  &.ant-input-affix-wrappexr:not(.ant-input-affix-wrapper-disabled):hover,
  &.ant-input-affix-wrapper-focused,
  .ant-input-affix-wrapper:focus {
    border: ${(props) => (props.isError ? `1px solid ${colors.s500}` : '')};
    box-shadow: ${(props) => (props.isError ? getBoxShadow('primary') : '')};
  }

  .ant-form-item-feedback-icon {
    margin-left: 13.5px;
  }

  width: ${(props) => props.width || '100%'};
`;
