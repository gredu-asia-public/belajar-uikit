import { InputProps as AntdInputProps } from 'antd';

export type SizeType = 'sm' | 'md' | 'lg';

export interface InputProps extends Omit<AntdInputProps, 'size'> {
  name?: string;
  width?: string;
  defaultValue?: number | string;
  value?: number | string;
  placeholder?: string;
  isDisabled?: boolean;
  prefixIcon?: React.ReactNode;
  suffixIcon?: React.ReactNode;
  type?: string;
  background?: string;
  size?: SizeType;
}
