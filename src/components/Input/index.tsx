import React from 'react';

import { StyledInput, StyledInputPassword } from './style';
import { InputProps } from './type';

export const Input: React.FC<InputProps> = ({
  name,
  width,
  defaultValue,
  value,
  placeholder,
  prefixIcon,
  suffixIcon,
  isDisabled,
  onChange,
  type,
  background,
  size = 'md',
  ...restProps
}) => {
  const Input = type === 'password' ? StyledInputPassword : StyledInput;
  return (
    <Input
      defaultValue={defaultValue}
      value={value}
      placeholder={placeholder}
      onChange={onChange}
      name={name}
      prefix={prefixIcon}
      suffix={suffixIcon}
      disabled={isDisabled}
      width={width}
      type={type}
      background={background}
      sizeType={size}
      {...restProps}
    />
  );
};

export default Input;
