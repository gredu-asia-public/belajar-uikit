import { Carousel } from "antd";
import styled from "styled-components";

import colors from "../../constants/colors";

export const StyledCarousel = styled(Carousel)`
  overflow: unset !important;

  && {
    text-align: center;
    background: none;
    height: auto;
    line-height: unset;
    .slick-dots-bottom {
      bottom: -5rem;
    }
    .slick-dots li {
      margin: 0 0.2em;
    }
    .slick-dots li button {
      font-size: 1em;
      height: 0.8rem;
      border-radius: 0.2em;
      width: 0.8rem;
      background: ${colors.p100};
      opacity: 1;
    }
    .slick-dots li.slick-active button {
      background: ${colors.p500};
      width: 2.4rem;
      height: 0.8rem;
    }
  }
`;
