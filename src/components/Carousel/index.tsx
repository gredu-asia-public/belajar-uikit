import React from "react";

import { CarouselProps } from "../../interfaces/components/carousel";

import { StyledCarousel } from "./styled";

const Carousel: React.FC<CarouselProps> = ({ children, ...restProps }) => (
  <StyledCarousel {...restProps}>{children}</StyledCarousel>
);

export default Carousel;
