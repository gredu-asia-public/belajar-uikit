import styled, { css } from "styled-components";

import { colors } from "../../constants/index";
import getFontStyle from "../../utils/style/getFontStyle";

import type { StyledTextProps } from "../../interfaces/components/text";

export const StyledText = styled.span<StyledTextProps>`
  ${(props) =>
    props.type ? getFontStyle(props.type) : getFontStyle("bodyDefault16")}
  color: ${(props) => (props.color ? colors[props.color] : colors.n900)};
  text-align: ${(props) => props.align};
  word-break: ${(props) =>
    props?.wordBreak ? props?.wordBreak : "break-word"};
  width: ${(props) => props.width};

  ${(props) =>
    props.withEllipsis &&
    `
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
  `}

  ${(props) =>
    props.link &&
    css`
      &:hover {
        color: ${colors.p500};
      }
    `}
`;
