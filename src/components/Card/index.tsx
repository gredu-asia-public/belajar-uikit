import * as React from "react";

import { StyledCard } from "./styled";

import type { CardProps } from "../../interfaces/components/card";

const Card: React.FC<CardProps> = ({
  children,
  withRadius,
  bordered = true,
  withBoxShadow,
  className,
  id,
  width,
  padding = "16px",
  cursor = "auto",
}) => {
  return (
    <StyledCard
      bordered={bordered}
      withRadius={withRadius}
      withBoxShadow={withBoxShadow}
      className={className}
      width={width}
      id={id}
      padding={padding}
      cursor={cursor}
    >
      {children}
    </StyledCard>
  );
};

export default Card;
