import { Card } from "antd";
import styled from "styled-components";

import colors from "../../constants/colors";
import getBoxShadow from "../../utils/style/getBoxShadow";

import type { CardProps } from "../../interfaces/components/card";

export const StyledCard = styled(Card)<CardProps>`
  border-radius: ${(props) => (props.withRadius ? "4px" : "0px")};
  border: ${(props) => (props.bordered ? `1px solid ${colors.n150}` : "0px")};
  width: ${(props) => props.width || "100%"};
  &:hover {
    box-shadow: ${(props) => (props.withBoxShadow ? getBoxShadow("card") : "")};
    transition: all 0.3s ease-in-out;
  }
  cursor: ${(props) => props.cursor};
  .ant-card-body {
    padding: ${(props) => props.padding};
  }
`;
