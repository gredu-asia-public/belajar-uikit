import React from "react";
import { Col } from "antd";

import type { ColumnProps } from "../../interfaces/components/column";

const Column: React.FC<ColumnProps> = ({ children, ...restProps }) => {
  return <Col {...restProps}>{children}</Col>;
};

export default Column;
