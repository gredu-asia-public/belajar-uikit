import styled from "styled-components";
import colors from "../../constants/colors";

export const LoadingLayout = styled.div`
  user-select: none;
  width: 100vw;
  height: 100vh;
  position: fixed;
  display: flex;
  align-items: center;
  min-height: 400px;
  justify-content: space-between;
  flex-direction: column;
  top: 0;
  left: 0;
  background: ${colors.n0};
  z-index: 99999;
`;

export const LoadingContent = styled.img`
  width: 100%;
  max-width: 300px !important;
  min-width: 300px !important;
  max-height: 300px !important;
  min-height: 300px !important;
`;
