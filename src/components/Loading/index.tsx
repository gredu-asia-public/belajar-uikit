import React from "react";
import LoadingIndicator from "../../public/assets/LoadingIndicator.gif";

import { MainLayout } from "../index";

import { LoadingLayout, LoadingContent } from "./styled";

interface LoadingProps {
  isLoading: boolean;
}

const Loading: React.FC<LoadingProps> = ({ isLoading }) => {
  return (
    <>
      {isLoading && (
        <LoadingLayout>
          <MainLayout>
            <LoadingContent src={LoadingIndicator} />
          </MainLayout>
        </LoadingLayout>
      )}
    </>
  );
};
export default Loading;
