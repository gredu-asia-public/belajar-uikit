import styled, { StyledComponent } from "styled-components";
import { Input } from "antd";
import colors from "../../constants/colors";
import getFontStyle from "../../utils/style/getFontStyle";
import getBorderRadius from "../../utils/style/getBorderRadius";
import { TextAreaProps as AntdTextAreaProps } from "antd/lib/input";
import { ForwardRefExoticComponent, RefAttributes } from "react";
import { TextAreaRef } from "antd/lib/input/TextArea";

const { TextArea } = Input;

export interface TextAreaProps extends AntdTextAreaProps {
  isError?: boolean;
}

export const StyledInput: StyledComponent<
  ForwardRefExoticComponent<
    AntdTextAreaProps & RefAttributes<TextAreaRef> & TextAreaProps
  >,
  any,
  TextAreaProps,
  never
> = styled(TextArea)<TextAreaProps>`
  border: 1px solid ${colors.n200};
  background-color: ${(props) => props.disabled && colors.n50} !important;
  ${getBorderRadius()};
  width: 100%;
  max-height: 128px !important;
  resize: none;
  padding: 12px 12px 36px;
  margin: 0;
  transition: all 0.3s;
  position: relative;

  textarea.ant-input {
    ${getFontStyle("bodyDefault16")}
    resize: none;
    border: none;
    height: 100%;
    min-height: 80px !important;
    padding: 0;
    ${getBorderRadius()};

    &:focus {
      box-shadow: none !important;
    }
  }

  input.ant-input[disabled] {
    background-color: ${colors.n50} !important;
  }

  textarea.ant-input[disabled] {
    background-color: ${colors.n50} !important;
  }

  &.ant-input-textarea-show-count:after {
    position: absolute;
    bottom: 12px;
    right: 12px;
    ${getFontStyle("caption")}
    color: ${(props) => (props.isError ? colors.ar500 : colors.n300)};
    background: transparent;
    display: ${(props) => (props.maxLength ? "block" : "none")};
    margin-bottom: 0;
  }

  &.ant-form-item-label {
    color: ${colors.n500} !important;
  }

  &:focus-within {
    box-shadow: 0 0 0 2px ${colors.p100} !important;
    border-color: ${colors.p400};
    outline: 0;
  }

  &:hover {
    border-color: ${(props) => !props.disabled && colors.p400};
  }

  &.ant-input-textarea-status-error {
    border-color: ${colors.ar400};
  }

  &.ant-input-textarea-status-error:focus-within {
    box-shadow: 0 0 0 2px ${colors.ar200} !important;
  }
`;
