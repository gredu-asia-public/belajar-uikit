import React, { useState } from "react";

import { StyledInput } from "./styled";
import { TextAreaProps } from "../../interfaces/components/textarea";

export const Input: React.FC<TextAreaProps> = ({
  value,
  onChange,
  showCount = true,
  maxLength = 140,
  name,
  isError,
  ...restProps
}) => {
  const [textValue, setValue] = useState<string>("");

  const triggerChange = (changedValue: string) => {
    onChange?.(changedValue);
  };

  const onChangeHandler = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const newValue = e.target.value;
    setValue(newValue);
    triggerChange(newValue);
  };

  return (
    <>
      <StyledInput
        name={name}
        value={value || textValue}
        showCount={showCount}
        maxLength={showCount ? maxLength : undefined}
        onChange={onChangeHandler}
        isError={isError}
        {...restProps}
      />
    </>
  );
};

export default Input;
