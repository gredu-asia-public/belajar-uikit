import { notification } from "antd";

import SnackBarContent from "./SnackBar";
import colors from "../../constants/colors";

import type {
  SnackBarProps,
  snackBarVariant,
} from "../../interfaces/components/snackbar";

export default function snackBar(params: SnackBarProps): void {
  const key = `open${Date.now()}`;

  const snackBarVariant: snackBarVariant = {
    warning: {
      backgroundColor: colors.aw200,
      iconColor: colors.aw800,
      borderColor: colors.aw800,
      textColor: "n900",
      closeIconColor: colors.n900,
    },
    success: {
      backgroundColor: colors.as50,
      iconColor: colors.as700,
      borderColor: colors.as700,
      textColor: "n900",
      closeIconColor: colors.n900,
    },
    danger: {
      backgroundColor: colors.ar100,
      iconColor: colors.ar700,
      borderColor: colors.ar700,
      textColor: "n900",
      closeIconColor: colors.n900,
    },
    info: {
      backgroundColor: colors.ai50,
      iconColor: colors.ai700,
      borderColor: colors.ai700,
      textColor: "n900",
      closeIconColor: colors.n900,
    },
    neutral: {
      backgroundColor: colors.n900,
      iconColor: colors.n0,
      borderColor: colors.n900,
      textColor: "n0",
      closeIconColor: colors.n0,
    },
  };

  const notificationStyle = {
    padding: "0px",
    width: "100%",
    height: "100%",
    minHeight: "fit-content",
    borderRadius: "4px",
    boxShadow: "inherit",
    border: "solid 1px",
    borderColor: snackBarVariant[params.type].borderColor,
    backgroundColor: snackBarVariant[params.type].backgroundColor,
  };

  return notification.open({
    key,
    duration: 4,
    style: notificationStyle,
    placement: params.placement || "top",
    onClose: params?.onClose,
    message: SnackBarContent({
      key,
      notification,
      type: params.type,
      title: params?.title,
      showIcon: params.showIcon,
      buttonText: params.buttonText,
      description: params.description,
      iconColor: snackBarVariant[params.type].iconColor,
      textColor: snackBarVariant[params.type].textColor,
      closeIconColor: snackBarVariant[params.type].closeIconColor,
      onClickButton: () => {
        if (params.onClickButton) params.onClickButton();
        return notification.close(key);
      },
    }),
  });
}
