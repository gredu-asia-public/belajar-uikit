import { Alert as AlertAntd } from "antd";
import styled, { StyledComponent } from "styled-components";

import type { StyledSnackBarProps } from "../../interfaces/components/snackbar";

export const StyledAlert: StyledComponent<
  any,
  any,
  StyledSnackBarProps
> = styled(AlertAntd)<StyledSnackBarProps>`
  background-color: inherit;
  border: none;
  padding: 14px 16px;

  .ant-alert-action {
    align-self: center;
    margin-left: 0;
  }

  .ant-alert-content {
    margin-right: 32px;
  }

  .ant-alert-message {
    margin-bottom: 0;
  }
`;

export const WrappedIcon = styled.span`
  cursor: pointer;
`;
