import React from "react";

import { Text, Button, Icon } from "../index";

import { StyledAlert, WrappedIcon } from "./styled";

import type { AlertProps } from "../../interfaces/components/snackbar";

const SnackBar: React.FC<AlertProps> = ({
  title,
  onClickButton,
  showIcon = false,
  type = "info",
  iconColor,
  textColor,
  notification,
  key,
  closeIconColor,
  buttonText,
  description,
}) => {
  const customIcon = (type: string) => {
    switch (type) {
      case "success":
        return <Icon color={iconColor} name="check_circle" />;
      case "danger":
        return <Icon color={iconColor} name="error" />;
      case "warning":
        return <Icon color={iconColor} name="warning" />;
      case "info":
        return <Icon color={iconColor} name="info" />;
      case "neutral":
        return <Icon color={iconColor} name="error" />;
      default:
        return null;
    }
  };

  return (
    <>
      <StyledAlert
        hasButton={!!buttonText}
        showIcon={showIcon}
        icon={customIcon(type)}
        message={
          <Text color={textColor} type="bodyDefault14Bold">
            {title}
          </Text>
        }
        description={description}
        action={
          buttonText ? (
            <Button size="sm" type="secondary" onClick={onClickButton}>
              {buttonText}
            </Button>
          ) : (
            <WrappedIcon onClick={() => notification.close(key)}>
              <Icon color={closeIconColor} name="clear" size="16px" />
            </WrappedIcon>
          )
        }
      />
      <style>{`
        .ant-notification-notice-close {
          margin-left: 10px;
          display: none;
        }

        .ant-notification-notice-message {
          margin: 0;
        }

        .ant-notification-notice-closable .ant-notification-notice-message {
          padding: 0 !important;
        }
      `}</style>
    </>
  );
};

export default SnackBar;
