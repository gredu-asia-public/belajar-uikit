import { Modal } from "antd";

import type { ModalConfirmProps } from "../../interfaces/components/modal";

const { confirm } = Modal;
const ModalConfirm = ({
  title,
  icon,
  content,
  onOk,
  onCancel,
  height = 164,
  ...restProps
}: ModalConfirmProps) =>
  confirm({
    title,
    icon,
    content,
    onOk,
    onCancel,
    wrapClassName: "modal-confirm",
    width: 540,
    style: { height },
    ...restProps,
  });

export default ModalConfirm;
