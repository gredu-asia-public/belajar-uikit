import React from "react";

import { Text, FlexBox, Label } from "../index";
import modalSize from "../../constants/modalSize";

import { ModalTitleContainer } from "./styled";

import type { ModalHeader as ModalHeaderType } from "../../interfaces/components/modal";

const ModalHeader: React.FC<ModalHeaderType> = ({
  title,
  subTitle,
  size = "md",
  label,
  icon,
}) => {
  return (
    <ModalTitleContainer>
      <FlexBox alignItems="center" columnGap={12}>
        {icon && icon}
        <Text type={modalSize[size].headerTitle}>{title}</Text>
        {label && (
          <Label type={label.type} size={label.size}>
            {label.text}
          </Label>
        )}
      </FlexBox>
      {subTitle && <Text type="bodyDefault16">{subTitle}</Text>}
    </ModalTitleContainer>
  );
};

export default ModalHeader;
