import React from "react";

import ModalTitle from "./ModalTitle";

import { StyledModal } from "./styled";

import type { ModalProps } from "../../interfaces/components/modal";

const Modal: React.FC<ModalProps> = ({
  children,
  title,
  subTitle,
  visible,
  footer,
  centered,
  onCancel,
  onOk,
  size = "md",
  withHeader = true,
  withClose = true,
  icon,
  label,
  ...rest
}) => {
  return (
    <StyledModal
      closable={withClose}
      title={
        withHeader && (
          <ModalTitle
            size={size}
            title={title}
            subTitle={subTitle}
            icon={icon}
            label={label}
          />
        )
      }
      visible={visible}
      footer={footer}
      centered={centered}
      onCancel={onCancel}
      onOk={onOk}
      size={size}
      {...rest}
    >
      {children}
    </StyledModal>
  );
};

export default Modal;
