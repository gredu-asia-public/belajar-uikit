import styled from "styled-components";
import { Modal } from "antd";

import getBorderRadius from "../../utils/style/getBorderRadius";
import getModalSize from "../../utils/style/getModalSize";

import type { ModalProps } from "../../interfaces/components/modal";

export const StyledModal = styled(Modal)<ModalProps>`
  ${getBorderRadius("default")};
  ${({ size }) => getModalSize(size)}
  .ant-modal-footer .ant-btn+.ant-btn:not(.ant-dropdown-trigger) {
    margin: 0;
  }
`;

export const ModalTitleContainer = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 8px;
`;
