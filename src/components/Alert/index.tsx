import React from "react";

import { Text, FlexBox } from "../";
import Icon from "../Icon";
import { alertVariant } from "../../constants/index";

import { StyledAlert } from "./styled";

import type { AlertProps, AlertType } from "../../interfaces/components/alert";

const Alert: React.FC<AlertProps> = ({
  title,
  onClose,
  showIcon = false,
  variant = "danger",
  description,
  closable = false,
}) => {
  const customIcon = (variant: AlertType) => {
    switch (variant) {
      case "success":
        return (
          <Icon color={alertVariant[variant].iconColor} name="check_circle" />
        );
      case "danger":
        return <Icon color={alertVariant[variant].iconColor} name="error" />;
      case "warning":
        return <Icon color={alertVariant[variant].iconColor} name="warning" />;
      case "info":
        return <Icon color={alertVariant[variant].iconColor} name="info" />;
      default:
        return null;
    }
  };

  return (
    <>
      <StyledAlert
        variant={variant}
        message={
          <>
            <FlexBox>
              {showIcon && customIcon(variant)}
              <Text
                color={alertVariant[variant].textColor}
                type="bodyDefault14Bold"
              >
                {title}
              </Text>
            </FlexBox>
            <Text type="bodyDefault14" color="n900">
              {description}
            </Text>
          </>
        }
        closable={closable}
        onClose={onClose}
      />
    </>
  );
};

export default Alert;
