import { Alert } from "antd";
import styled, { StyledComponent } from "styled-components";

import getAlertVariant from "../../utils/style/getAlertVariant";
import { colors } from "../../constants/index";

import type { StyledAlertProps } from "../../interfaces/components/alert";

export const StyledAlert: StyledComponent<any, any, StyledAlertProps> = styled(
  Alert
)<StyledAlertProps>`
  ${getAlertVariant};
  width: 100%;

  svg {
    margin-right: 12px;
  }
  &.ant-alert {
    padding: 16px;
  }
  &.ant-alert-message {
    line-height: 22px;
  }
  .ant-alert-close-icon .anticon-close {
    color: ${colors.n900};
  }
`;
