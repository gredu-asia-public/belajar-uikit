import React from "react";
import { Row as RowAntd } from "antd";

import type { RowProps } from "../../interfaces/components/row";

const Row: React.FC<RowProps> = ({ children, ...restProps }) => {
  return <RowAntd {...restProps}>{children}</RowAntd>;
};

export default Row;
