import {
  ILoginSsoResponse,
  ILoginSsoResponseMapped,
} from "../../interfaces/services/Login";
import { loginToJson } from "../../utils/attributeMapper/login";
import customAxios from "../../utils/client";

const refreshingToken = async (
  refreshToken: string
): Promise<ILoginSsoResponseMapped> => {
  try {
    const response = await customAxios.post<ILoginSsoResponse>(
      `/user/v1.0/renew-token`,
      { refresh_token: refreshToken }
    );
    return loginToJson(response.data.data);
  } catch (error: any) {
    return error.response?.data;
  }
};

export { refreshingToken };
