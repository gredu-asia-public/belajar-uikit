import type { LabelSize } from "../interfaces/constants/labelSize";

const labelSize: LabelSize = {
  sm: { padding: "2px 8px", fontStyle: "caption" },
  md: { padding: "2px 10px", fontStyle: "bodyDefault14" },
  lg: { padding: "4px 12px", fontStyle: "bodyDefault14" },
};

export default labelSize;
