import type { ModalSize } from "../interfaces/constants/modalSize";

const modalSize: ModalSize = {
  classSetting: {
    headerPadding: "24px 0 24px 24px",
    headerTitle: "title2Bold",
    bodyWidth: "780px",
    footerPadding: "16px 24px 24px 0",
  },
  xl: {
    headerPadding: "24px 0 24px 24px",
    headerTitle: "title3Bold",
    bodyWidth: "1080px",
    footerPadding: "16px 24px 24px 0",
  },
  md: {
    headerPadding: "24px 0 24px 24px",
    headerTitle: "title2Bold",
    bodyWidth: "740px",
    footerPadding: "16px 24px 24px 0",
  },
  sm: {
    headerPadding: "20px 0 16px 24px",
    headerTitle: "title3Bold",
    bodyWidth: "480px",
    footerPadding: "16px 24px 16px 0",
  },
};

export default modalSize;
