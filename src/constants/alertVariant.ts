import colors from "./colors";

import type { AlertVariant } from "../interfaces/components/alert";

const alertVariant: AlertVariant = {
  warning: {
    backgroundColor: colors.ad50,
    iconColor: colors.ad500,
    borderColor: colors.ad500,
    textColor: "n900",
    closeIconColor: colors.n900,
  },
  success: {
    backgroundColor: colors.as50,
    iconColor: colors.as700,
    borderColor: colors.as700,
    textColor: "n900",
    closeIconColor: colors.n900,
  },
  danger: {
    backgroundColor: colors.ar100,
    iconColor: colors.ar700,
    borderColor: colors.ar700,
    textColor: "n900",
    closeIconColor: colors.n900,
  },
  info: {
    backgroundColor: colors.ai50,
    iconColor: colors.ai700,
    borderColor: colors.ai700,
    textColor: "n900",
    closeIconColor: colors.n900,
  },
};

export default alertVariant;
