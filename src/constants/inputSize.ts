import { InputSize } from '../interfaces/inputSize';

const inputSize: InputSize = {
  sm: {
    height: '40px',
  },
  md: {
    height: '44px',
  },
  lg: {
    height: '52px',
  },
};

export default inputSize;