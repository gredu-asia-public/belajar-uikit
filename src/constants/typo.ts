import type Typo from "../interfaces/typo";

const typo: Typo = {
  heading1: {
    size: "4.2rem",
    lineHeight: "4.8rem",
    weight: 700,
  },
  heading2: {
    size: "3.2rem",
    lineHeight: "4.2rem",
    weight: 700,
  },
  title1: {
    size: "2.8rem",
    lineHeight: "3.6rem",
    weight: 400,
  },
  title1Bold: {
    size: "2.8rem",
    lineHeight: "3.6rem",
    weight: 700,
  },
  title2: {
    size: "2.4rem",
    lineHeight: "3.2rem",
    weight: 400,
  },
  title2Bold: {
    size: "2.4rem",
    lineHeight: "3.2rem",
    weight: 700,
  },
  title3: {
    size: "2rem",
    lineHeight: "2.6rem",
    weight: 400,
  },
  title3Bold: {
    size: "2rem",
    lineHeight: "2.6rem",
    weight: 700,
  },
  title4: {
    size: "1.8rem",
    lineHeight: "2.4rem",
    weight: 400,
  },
  title4SemiBold: {
    size: "1.8rem",
    lineHeight: "2.4rem",
    weight: 600,
  },
  title4Bold: {
    size: "1.8rem",
    lineHeight: "2.4rem",
    weight: 700,
  },
  bodyDefault16: {
    size: "1.6rem",
    lineHeight: "2rem",
    weight: 400,
  },
  bodyDefault16Longer: {
    size: "1.6rem",
    lineHeight: "2.4rem",
    weight: 400,
  },
  bodyDefault16SemiBold: {
    size: "1.6rem",
    lineHeight: "2rem",
    weight: 600,
  },
  bodyDefault16Bold: {
    size: "1.6rem",
    lineHeight: "2rem",
    weight: 700,
  },
  bodyDefault14: {
    size: "1.4rem",
    lineHeight: "2rem",
    weight: 400,
  },
  bodyDefault14Longer: {
    size: "1.4rem",
    lineHeight: "2.2rem",
    weight: 400,
  },
  bodyDefault14SemiBold: {
    size: "1.4rem",
    lineHeight: "2rem",
    weight: 600,
  },
  bodyDefault14Bold: {
    size: "1.4rem",
    lineHeight: "2.2rem",
    weight: 700,
  },
  caption: {
    size: "1.2rem",
    lineHeight: "1.6rem",
    weight: 400,
  },
  captionBold: {
    size: "1.2rem",
    lineHeight: "1.6rem",
    weight: 700,
  },
  captionSmall: {
    size: "1rem",
    lineHeight: "1.6rem",
    weight: 400,
  },
  captionSmallBold: {
    size: "1rem",
    lineHeight: "1.6rem",
    weight: 700,
  },
  uppercaseCaption: {
    size: "1.2rem",
    lineHeight: "1.6rem",
    weight: 700,
    letterSpacing: "1.4px",
  },
  uppercaseCaptionSmall: {
    size: "1rem",
    lineHeight: "1.2rem",
    weight: 700,
    letterSpacing: "1.4px",
  },
};

export default typo;
