import colors from "./colors";

import type { BadgeState } from "../interfaces/constants/badgeState";

export const badgeState: BadgeState = {
  default: colors.n300,
  warning: colors.aw700,
  disclaimer: colors.as500,
  info: colors.ai500,
  error: colors.ar500,
  primaryStudent: colors.p500,
};
