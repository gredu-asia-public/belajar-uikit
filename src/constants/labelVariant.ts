import type { LabelVariant } from "../interfaces/constants/labelVariant";

import colors from "./colors";

export const labelVariant: LabelVariant = {
  default: {
    color: colors.n50,
    fontColor: colors.n700,
    border: `solid 1px ${colors.n200}`,
  },
  warning: { color: colors.aw100, fontColor: colors.aw700, border: "none" },
  disclaimer: { color: colors.as50, fontColor: colors.as500, border: "none" },
  info: { color: colors.ai50, fontColor: colors.ai500, border: "none" },
  error: { color: colors.ar50, fontColor: colors.ar500, border: "none" },
  primary: { color: colors.p50, fontColor: colors.p500, border: "none" },
  success: { color: colors.as50, fontColor: colors.as500, border: "none" },
};
