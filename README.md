# Introduction

This is a collection of components, utility functions and config for gredu application

# Installation belajar-ui-kit

prerequisite

- yarn ^v1.22.11
- npm ^6.14.17
- node ^v14.17.6
  before to install ensure setup local registry to point out our gitlab registry

## NPM Command

```
echo @gredu-asia-public:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

then

```
npm install --save @gredu-asia-public/belajar-uikit
```

## Yarn Command

```
echo @gredu-asia-public:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

then

```
yarn add --save @gredu-asia-public/belajar-uikit
```

## Usage

```
import {Avatar} from '@gredu-asia-public/belajar-uikit';

<Avatar size="sm/md/lg" />
```

Imports may variate depends on which components/utils/constant you want to use. usage recomended using Typescript for autocompelete on library props.

## Components

- Avatar
- Alert
- Button
- Badge
- Card
- Carousel
- Column
- Datepicker
- Divider
- EmptyState
- FlexBox
- Icon
- Label
- Layout
- Loading
- Modal
- Popover
- Row
- SnackBar
- Text
- TextArea
- Tooltip

## Utils

- client
- firebaseLogEvent
- dateFormat

## Constants

- alertVariant
- avatarSize
- buttonVariant
- buttonSize
- borderRadius
- boxShadow
- colors
- typo

Maintainer
MIT © rihco.aditya
